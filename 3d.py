from matplotlib import cm
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as image

#python 3.8.3
#Matplotlib
#numpy

def add_offset(points, offset):
    return np.array([point + offset for point in points])

def image_to_texture(img):
    return np.fliplr(img.transpose((1,0,2)))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
img = image.imread(r'texture1.png')

#Genera la esfera usando las formulas:
# x = ρsin ϕcosθ
# y = ρsin ϕsinθ
# z = ρcosϕ
u = np.linspace(0, 2 * np.pi, 100) #ϕ
v = np.linspace(0, np.pi, 100) #θ

x = 1 * np.outer(np.cos(u), np.sin(v)) 
y = 1 * np.outer(np.sin(u), np.sin(v))
z = 1 * np.outer(np.ones(np.size(u)), np.cos(v))

x2 = add_offset(1 * np.outer(np.cos(u), np.sin(v)), 1.5)
y2 = add_offset(1 * np.outer(np.sin(u), np.sin(v)), 1.5)
z2 = add_offset(1 * np.outer(np.ones(np.size(u)), np.cos(v)), 1.5)


x3 = add_offset(1 * np.outer(np.cos(u), np.sin(v)), -1.5)
y3 = add_offset(1 * np.outer(np.sin(u), np.sin(v)), -1.5)
z3 = 0 * x3

#Para rellenar el poligono con un color se usa color
ax.plot_surface(x, y, z, rstride=4, cstride=4, color='b', alpha=1)
#Para rellenar el poligono con un degradado se usa cmap
ax.plot_surface(x2, y2, z2, rstride=4, cstride=4, cmap='plasma', alpha=1)
#Para rellenar con una textura se usa facecolors
ax.plot_surface(x3, y3, z3, rstride=4, cstride=4, facecolors=image_to_texture(img), alpha=1)


plt.show()



